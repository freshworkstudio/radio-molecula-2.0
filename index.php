<?php 
$server_url = "s3.chileservidores.cl:7456";
$banner_top = "images/logo.png";
$banner_bottom = "images/banner.gif";
$share_twitter = "Escucho en @molecula_cl a SONG | "; //LA url la pone al final Twitter solo.
$share_facebook = "Estoy escuchando a SONG. Escúchalo tu también en @molecula_cl #molecula";
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title>Radio Molécula</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<script src="js/player.js"></script>
	<script src="js/jquery.js"></script>
	<link href="css/radio.css" rel="stylesheet" media="screen">

	<script>
	<!-- NOW PLAYING JS -->
	var nowplayingsong = false;
	var tmpnowplayingsong = false;
	var syncsegs = 5000; //5 segs - 5000 milisegs.
	var waitToChange = 41000; //41 segs - 41000 milisegs.
	$(function(){
		//Al inicial la radio, actualizar nowplaying e history
		syncNowPlaying(true); 
		syncHistory(true);
	});
	function syncHistory(){
		//Actualiza el div .playhistory
		$.get('playhistory.php?randval='+ Math.random(),function(res){
			$(".playhistory .songs").html(res);
		});
	}
	function syncNowPlaying(initial){
		if(typeof(initial) == "undefined")initial = false;	
		$.get("nowplaying.php?randval="+ Math.random(),function(res){
			if(res != "FAIL"){
					if(res != nowplayingsong){
					nowplayingsong = res;
					$(document).trigger("app.changesong",[res,initial]);
					initial = false;
				}
			}else{
				initial = true;	
			}
			setTimeout(function(){
				syncNowPlaying(initial);
			},syncsegs);
		});
	}
	<!-- El evento "app.changesong" se ejecuta cuando el servidor reconoce un cambio de canción -->
	$(document).bind("app.changesong",function(ev,songName,initial){
		//Espera el numero de segundos indicado por var waitToChange para cmbiar la canción 
		var tmpsegs = (initial)?0:waitToChange;
		setTimeout(function(){
			//Al pasar los 40 segundos aprox de waitToChange, actualizamos el nombre y actualizamos historial
			changeSong(songName);
			syncHistory();
		},tmpsegs);
	});
	
	function changeSong(name){
		//funcion que permite cambiar el nombre de la canción donde y como corresponde. 
		tmpnowplayingsong = name;
		$(".nowplaying").fadeOut(0,function(){
			$(this).html(name).fadeIn(0);
		});
	}
	</script>
</head>

<body>
	<div id="container">
		<div id="header">
			<!-- Logo -->
			<div class="logo"><img src="<?php echo $banner_top; ?>" alt="Radio Molécula" /></div>
		</div>
		
		<div id="player-container">
			<div id="player">
				<script type="text/javascript">
					new WHMSonic({
					path : "WHMSonic.swf",
					source : "<?php echo $server_url; ?>",
					volume : 70,
					autoplay : true,
					width: 360,
					height: 65,
					
					});
		  		</script>
			</div>
			<div id="barra">
			</div>
			<!-- NOW PLAYING -->
			<div class="nowplaying"> </div>
		</div>
		
		
		<!-- PLAY HISTORY -->
		<div class="playhistory"> 
			<h3>Últimas canciones</h3>
			<div class="songs"></div>
			
			<div class="compartir">
				<span>Compartir: </span>
				<a class="twitter" href="http://twitter.com/share?url=<?php echo urlencode("http://www.molecula.cl"); ?>"><img src="images/twitter_icon.png" alt="Facebook" /></a>
				<a class="facebook" href=""><img src="images/facebook_icon.png" alt="Facebook" /></a>
			</div>
		</div>
		
		<footer>	
			<!-- AC� VA EL BANNER INFERIOR -->
			<div class="banner">
				<a target="_blank" title="Miguel Torres" href="https://www.facebook.com/migueltorresenchile">
					<img src="<?php echo $banner_bottom; ?>" alt="banner_cl" />
				</a>
			</div>
		</footer>
	</div>
	
	<div id="fb-root"></div>
	<script>
	  window.fbAsyncInit = function() {
		FB.init({
		  appId      : '312720282073923', // App ID
		  status     : true, // check login status
		  cookie     : true, // enable cookies to allow the server to access the session
		  oauth      : true, // enable OAuth 2.0
		  xfbml      : true  // parse XFBML
		});
	
		// Additional initialization code here
	  };
	
	  // Load the SDK Asynchronously
	  (function(d){
		 var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
		 js = d.createElement('script'); js.id = id; js.async = true;
		 js.src = "//connect.facebook.net/es_LA/all.js";
		 d.getElementsByTagName('head')[0].appendChild(js);
	   }(document));
	</script>
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-26858919-1']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
	</script>
	<script>
$('.twitter').click(function(event) {
	var width  = 575,
		height = 400,
		left   = ($(window).width()  - width)  / 2,
		top    = ($(window).height() - height) / 2,
		url    = this.href,
		opts   = 'status=1' +
				 ',width='  + width  +
				 ',height=' + height +
				 ',top='    + top    +
				 ',left='   + left;
	
	url += "&text=<?php echo (str_replace("SONG","\"+tmpnowplayingsong+\"",urlencode($share_twitter))); ?>";
	window.open(url, 'twitte', opts);
	
	return false;
	});
	
	$('.facebook').click(function(event) {
	FB.ui({
		method: 'feed',
		name: "<?php echo (str_replace("SONG","\"+tmpnowplayingsong+\"",($share_facebook))); ?>",
		link: 'http://www.molecula.cl/'
	},function(response) {
	
	}); 
	
	return false;
	});
	</script>
	

	
</body>
</html>