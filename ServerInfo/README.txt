encode_utf8.php		Contains an example of how to manipulate the encoding
error.php		Contains an example of the effect of the different levels of error-reporting
			Please note that this only applies to exceptions that occur during the retrieval
			of information from the radiostation.

fields.php		Contains an example how you can manipulate which information about the server
			is shown

output_dl.php		Contains an example of output as a DefinitionList (<dl></dl>)
output_html.php		Contains an example of output in a html-table (<table></table>)
output_ol.php		Contains an example of output in an OrderedList (<ol></ol>)
output_php.php		Contains an example of php output
output_ul.php		Contains an example of output in an UnorderedList (<ul></ul>)
output_xml.php		Contains an example of xml output

simple.php		Contains the most basic example

template.php		Contains an example of how you can create your own templates for the outout
			of data

urls.php		Contains an example of how to manipulate the creation of hyperlinks