<?php
/**
 * Excudo InternetRadio
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://devshed.excudo.net/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to marty@excudo.net so we can send you a copy immediately.
 *
 * @category   Excudo
 * @package    InternetRadio
 * @copyright  Copyright (c) 2005-2010 Excudo. (http://www.excudo.net)
 * @license    http://devshed.excudo.net/license/new-bsd     New BSD License
 */

/**
 * @see InternetRadio_Abstract
 */
require_once "Abstract.php";

/**
 * Class that can parse and display the information of Shoutcast Servers
 */
class InternetRadio_Shoutcast extends InternetRadio_Abstract
{
	/**
	 * Used by getServerType() to show what kind of internet-radiostations this class
	 * can handle
	 */
	const SERVER_TYPE	= "Shoutcast";

	/**
	 * The following constants all serve the function which parses the public information
	 * pages. This way, it is easy to change in case the format of these pages change in
	 * the future
	 */
	/**
	 * Determines from which point we start searching for the fields with server information
	 * @see parseFields();
	 */
	const OFFSET		= "Current Stream Information";
	/**
	 * This is how the html-table with the track-history starts
	 * @see parseHistory();
	 * @see TABLE_END
	 */
	const TABLE_START	= "<table border=0 cellpadding=2 cellspacing=2>";
	/**
	 * End this is how it ends
	 * @see parseHistory();
	 * @see TABLE_START
	 */
	const TABLE_END		= "</table>";


	/**
	 * All the possible fields with information about the stream/server that can be loaded
	 * By default, this array is assigned to the $fields (see parent class) array. But, the
	 * $fields array can also be set with setFields() in order to display a particular set
	 * of fields
	 *
	 * @var array
	 *
	 * @see $fields
	 * @see setFields()
	 */
	protected $defaultFields = array(
					"Server Status" => "n/a",
					"Stream Status" => "n/a",
					"Listener Peak" => "n/a",
					"Average Listen Time" => "n/a",
					"Stream Title" => "n/a",
					"Content Type" => "n/a",
					"Stream Genre" => "n/a",
					"Stream URL" => "n/a",
					"Current Song" => "n/a"
					);

	/**
	 * The default streamChunks which are loaded when setStreamChunks() is called without
	 * argument (which is done in the constructor of the Abstract parent classs)
	 *
	 * @var array
	 */
	private $defaultStreamChunks = array(
		"history"	=> -1,
		);
	/**
	 * By default it will be assigned $defaultStreamChunks as value.
	 *
	 * @see setStreamChunks()
	 *
	 * @var array
	 */
	protected $streamChunks;

	/**
	 * The default pageMaps which is loaded when setPageMap() is called without
	 * argument (which is done in the constructor of the Abstract parent classs)
	 *
	 * @var array
	 */
	private $defaultPageMap = array(
		"stream"	=> "/",
		"history"	=> "/played.html",
		);
	/**
	 * By default it will be assigned $defaultPageMap as value.
	 *
	 * @see setPageMap()
	 *
	 * @var array
	 */
	protected $pageMap;

	/**
	 * Implementation of abstract parent methods
	 */

	/**
	 * Returns the type of InternetRadiostation this class can handle
	 *
	 * @return String
	 */
	public function getServerType()
	{
		return self::SERVER_TYPE;
	}

	/**
	 * When called without argument it sets the default pageMap.
	 * The page map links
	 *
	 * @see parent::setPageMap()
	 *
	 * @param array $data	Array containing the mapping
	 *
	 * @return void
	 */
	public function setPageMap($data = null)
	{
		if (is_null($data))
			$data = $this->defaultPageMap;
		$this->pageMap = $data;
	}

	/**
	 * When called without argument it sets the default streamChunks.
	 *
	 * @see parent::setStreamChunks()
	 *
	 * @param array $data	Array containing the mapping
	 *
	 * @return void
	 */
	protected function setStreamChunks($data = null)
	{
		if (is_null($data))
			$data = $this->defaultStreamChunks;
		$this->streamChunks = $data;
	}

	/**
	 * Retrieves the page with the information about the server/stream, parses it and puts the
	 * parsed data into the appropriate fields
	 *
	 * @see getStreamContents()
	 * @see loadStatus()
	 *
	 * @return void
	 */
	public function parseFields($page = null)
	{
		if ($contents = $this->getStreamContents($page))
		{
			 // parsing the contents
			$very_first_pos = stripos($contents, self::OFFSET);

			foreach ($this->fields AS $item => $value)
			{
				$first_pos		= stripos($contents, $item, $very_first_pos);
				$line_start		= strpos($contents, "<td>", $first_pos);
				$line_end		= strpos($contents, "</td>", $line_start) + 4;
				$difference		= $line_end - $line_start;
				$line			= substr($contents, $line_start, $difference);

				$this->fields[$item]	= strip_tags($line);

				if ($this->createHyperlinks && strtolower(substr($item, -3)) == "url")
				{
					$this->fields[$item] = "<a href=\"".$this->fields[$item]."\">".$this->fields[$item]."</a>";
				}
			}
		}
	}

	/**
	 * This function will be called from the parent's constructor
	 * By setting the contentArr, it serves two purposes:
	 * 1. The keys of the array can later be used in validations that check if the
	 *    index is an existing one (so, an existing key of this array)
	 * 2. It initializes the array with null values. This way the caching-functions
	 *    will be able to tell if content has not been loaded or if it has been loaded
	 *    with empty data
	 *
	 * @see parent::contentArr
	 * @see parent::__construct()
	 *
	 * @return void
	 */
	protected function setContentArr()
	{
		$this->contentArr = array(
			"stream"	=> null,
			"history"	=> null,
			);
	}

	/**
	 * overloaded methods
	 */

	/***
	 * Overloads the parents function in order to cut of part of the output which is irrelevant
	 *
	 * @var String $page		Optional. The page where we can find the content.
	 *							If none is given, the $pageMap variable will be used
	 *
	 * @see parent::loadStreamContents()
	 *
	 * @return void
	 */
	protected function loadContents($index, $page = null)
	{
		parent::loadContents($index, $page);
		if ($index == "stream")
		{
			if (!empty($this->contentArr['stream']))
			{
				preg_match("/(Content-Type:)(.*)/i", $this->contentArr['stream'], $matches);
				if (count($matches) > 0)
				{
					$contentType = trim($matches[2]);
					if ($contentType != "text/html")
					{
						throw new Exception("This is not a valid shoutcast-stream");
					}
				}
			}
		}
	}

	/**
	 * Other methods which are specific for this internet-radiostation
	 */

	/**
	 * Retrieves the history of the played tracks and returns it using the choosen
	 * (or default) template
	 *
	 * @param unknown_type $page				The page where the information about
	 * 							the stream can be found. If null, the
	 * 							default will be used. See
	 * @param InternetRadio_Output_Interface $template	The template that has to be used when
	 * 											upon returning the data
	 *
	 * @return mixed	The output as rendered by the template object. In most cases
	 * 			this will be a string, but it could just as easily be another
	 * 			type (for example, InternerRadio_Output_Php returns an array)
	 */
	public function getHistoryInfo($page = null, InternetRadio_Output_Interface $template = null)
	{
		
		if (empty($this->tracks))
		{
			try {
				$this->parseHistory($page);
			} catch (InternetRadio_Exception $e) {
				$this->error = $e->getMessage();
				switch ($this->exceptionReporting)
				{
					case self::EXCEPTION_THROW :
						throw $e;
						break;
					case self::EXCEPTION_SHOW :
						$this->tracks = array(array("error", $e->getMessage()));
						break;
					case self::EXCEPTION_HIDE :
					default :
						$this->tracks = array(array("error", "failed to load stream"));
						break;
				}
			}
		}
		return $this->getInfo($this->tracks, $template);
	}
	/**
	 * Same as getHistoryInfo($page, InternetRadio_Output_Html_Table()), but without the
	 * need to pass the template object as an argument
	 *
	 * @param String $page
	 *
	 * @see getHistoryInfo()
	 *
	 * @return mixed
	 */
	public function getHistoryInfoAsHtml($page = null)
	{
		require_once dirname(__FILE__)."/Output/Html/Table.php";
		return $this->getHistoryInfo($page, new InternetRadio_Output_Html_Table());
	}
	/**
	 * Same as getHistoryInfo($page, InternetRadio_Output_Php()), but without the
	 * need to pass the template object as an argument
	 *
	 * @param String $page
	 *
	 * @see getHistoryInfo()
	 *
	 * @return mixed
	 */
	public function getHistoryInfoAsPhp($page = null)
	{
		require_once dirname(__FILE__)."/Output/Php.php";
		return $this->getHistoryInfo($page, new InternetRadio_Output_Php());
	}
	/**
	 * Same as getHistoryInfo($page, InternetRadio_Output_Xml()), but without the
	 * need to pass the template object as an argument
	 *
	 * @param String $page
	 *
	 * @see getHistoryInfo()
	 *
	 * @return mixed
	 */
	public function getHistoryInfoAsXml($page = null)
	{
		require_once dirname(__FILE__)."/Output/Xml.php";
		return $this->getHistoryInfo($page, new InternetRadio_Output_Xml());
	}

	/**
	 * Retrieves the page with the information about the history of the played tracks,
	 * parses it and craetes an array of tracks out of the parsed data
	 *
	 * @param String $page		The location of the page with the content. This is the
	 * 							[page] part in http://[domain]:[port]/[page]
	 *
	 * @return void
	 */
	protected function parseHistory($page = null)
	{
		$html = $this->getHistoryContents($page);
		$fromPos	= stripos($html, self::TABLE_START);
		if (false == $fromPos)
		{
			// fix for newer shoutcastservers, which quote the values of the html-attributes
			$tableStart = preg_replace("/([0-2])/", "\"\\1\"", self::TABLE_START);
			$fromPos	= stripos($html, $tableStart);
		}
		$toPos		= stripos($html, self::TABLE_END, $fromPos);
		$tableData	= substr($html, $fromPos, ($toPos-$fromPos));
		$lines		= explode("</tr><tr>", $tableData);
		$tracks = array();
		$c = 0;
		foreach ($lines AS $line)
		{
			$info = explode ("</td><td>", $line);
			// OJO, aca formateo el formato de la hora mostrada.
			$timeold = trim(strip_tags($info[0]));
			$time = date ('H:i',strtotime($timeold));
			if (substr($time, 0, 9) != "Copyright" && !preg_match("/Tag Loomis, Tom Pepper and Justin Frankel/i", $info[1]))
			{
				$this->tracks[$c]['time'] = $time;
				$this->tracks[$c++]['track'] = trim(strip_tags($info[1]));
			}
		}
		if (count($this->tracks) > 0)
		{
			unset($this->tracks[0]);
			if (isset($this->tracks[1]))
				$this->tracks[1]['track'] = str_replace("Current Song", "", $this->tracks[1]['track']);
		}
	}

	/**
	 * Loads the contents of the information about the history of the played tracks
	 *
	 * @param String $page		The location of the page with the content. This is the
	 * 							[page] part in http://[domain]:[port]/[page]
	 *
	 * @throws InternetRadio_Exception
	 *
	 * @see loadContents()
	 *
	 * @return void
	 */
	protected function loadHistoryContents($page = null)
	{
		$this->loadContents("history", $page);
	}

	/**
	 * Wrapper for parent::getContents() with the first parameter filled correctly
	 *
	 * @param String $page		The location of the page with the content. This is the
	 * 							[page] part in http://[domain]:[port]/[page]
	 *
	 * @see getContents()
	 *
	 * @return String
	 */
	protected function getHistoryContents($page = null)
	{
		return $this->getContents("history", $page);
	}
}