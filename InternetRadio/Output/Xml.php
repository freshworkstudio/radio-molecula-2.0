<?php
/**
 * Excudo InternetRadio
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://devshed.excudo.net/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to marty@excudo.net so we can send you a copy immediately.
 *
 * @category   Excudo
 * @package    InternetRadio
 * @copyright  Copyright (c) 2005-2010 Excudo. (http://www.excudo.net)
 * @license    http://devshed.excudo.net/license/new-bsd     New BSD License
 */
 
/**
 * @see InternetRadio_Output_Tag
 */
require_once dirname(__FILE__)."/Tag.php";

/**
 * This template class generates xml-output
 */
class InternetRadio_Output_Xml extends InternetRadio_Output_Tag
{
	/**
	 * Template-string which can be handled by setHeader() and which is
	 * used as the default
	 * @see setHeader()
	 */
	const DEFAULT_HEADER	= "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<data>\n";
	/**
	 * Template-string which can be handled by setFooter() and which is
	 * used as the default
	 * @see setFooter()
	 */
	const DEFAULT_FOOTER	= "</data>";
	/**
	 * Template-string which can be handled by setRow() and which is
	 * used as the default
	 * @see setRow()
	 */
	const DEFAULT_ROW	= "\t<item>\n\t\t<name>%s</name>\n\t\t<value>%s</value>\n\t</item>\n";

	/**
	 * Constructor.
	 * Sets the header, footer and row templates
	 *
	 * @param string $header	Optional. If not given, the default will be used
	 * @param string $footer	Optional. If not given, the default will be used
	 * @param string $row		Optional. If not given, the default will be used
	 * 
	 * @see setHeader()
	 * @see setFooter()
	 * @see setRow()
	 */
	public function __construct($header = null, $footer = null, $row = null)
	{
		$this->setHeader(is_null($header) ? self::DEFAULT_HEADER : $header);
		$this->setFooter(is_null($footer) ? self::DEFAULT_FOOTER : $footer);
		$this->setRow(is_null($row) ? self::DEFAULT_ROW : $row);
	}
	
	/**
	 * Overloads the parent function so it can create valid xml-values
	 *
	 * @param array $data	Array which we want to convert into xml
	 *
	 * @see parent::render()
	 *
	 * @return String (xml)
	 */
	public function render(array $data)
	{
		$this->sanitizeData($data);

		$newData = array();
		foreach ($data AS $key => $value)
		{
			$newData[htmlspecialchars(utf8_encode($key))] = htmlspecialchars(utf8_encode($value));
		}
		return parent::render($newData);
	}
}