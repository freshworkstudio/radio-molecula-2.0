<?php
/**
 * Excudo InternetRadio
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://devshed.excudo.net/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to marty@excudo.net so we can send you a copy immediately.
 *
 * @category   Excudo
 * @package    InternetRadio
 * @copyright  Copyright (c) 2005-2010 Excudo. (http://www.excudo.net)
 * @license    http://devshed.excudo.net/license/new-bsd     New BSD License
 */
 
/**
 * @see InternetRadio_Output_Interface
 */
require_once dirname(__FILE__)."/Interface.php";

/**
 * This class serves as the base for all other InternetRadio_Output classes which generate output with tags
 * For example, output in html, xml, etc.
 */
class InternetRadio_Output_Tag implements InternetRadio_Output_Interface
{
	/**
	 * Top-part of the output, which is not variable
	 *
	 * @see getHeader()
	 * @see setHeader()
	 *
	 * @var String
	 */
	protected $_header;

	/**
	 * Bottom-part of the output, which is not variable
	 *
	 * @see getFooter()
	 * @see setFooter()
	 *
	 * @var String
	 */
	protected $_footer;

	/**
	 * Body-part of the output, which Is variable and occurs 1 + n times
	 *
	 * @see getRow()
	 * @see setRow()
	 *
	 * @var String
	 */
	protected $_row;

	/**
	 * This indicates that the data (to be rendered) is passed as key-value pairs
	 * (in which the key-part has significance too and needs to be shown)
	 *
	 * @var boolean
	 */
	protected $_keyValue;

	/**
	 * A positive value indicates that we need to skip a few lines before starting the output
	 *
	 * @see setOffset()
	 *
	 * @var integer
	 */
	protected $_offset;

	/**
	 * A positive value indicates we need to limit the amount of rows in the output
	 * -1 indicates there is no limit (and so does null)
	 *
	 * @see setLimit()
	 *
	 * @var integer
	 */
	protected $_limit;


	/**
	 *
	 *
	 * @return String
	 */
	public function render(array $data)
	{
		// initiate variable that will hold the variable-data
		$body = "";
		// initiate variable that will count the amount of rows we're parsing
		$count = 0;
		// initiate variable that will count the amount of rows we're adding as variable data
		$added = 0;
		// calculating the real limit we're gonna use
		if (!is_null($this->_limit) && $this->_limit < 0)
		{
			// setting the limit to 'limitless'
			$limit = count($data) - (int) $this->_offset + $this->_limit;
		}
		else
		{
			$limit = $this->_limit;
		}
		//print_r($data);
		//$this->sanitizeData($data);
		

		// looping through the data-array
		foreach ($data AS $k => $v){
			// check if we are within the boundaries defined by the offset & limit
			$key = $v['time'];
			$value = $v['track'];
			if ( (is_null($this->_offset) || $count >= $this->_offset) && (is_null($limit) || $added < $limit))
			{

				// if true, we want to display both key and value
				if ($this->_keyValue)
				{
					$body .= sprintf($this->_row, $key, $value);
				}
				else
				{
					$body .= sprintf($this->_row, $value);
				}
				// row has been added, we increment
				$added++;
			}
			else
			{
				echo "<!-- ".$count." is outside boundaries; offset[".$this->_offset."], limit[".$limit."] //-->\n";
			}
			// row has been parsed, we increment
			$count++;
		}
		// returning the rendered output
		return $this->getHeader().$body.$this->getFooter();
	}

	protected function sanitizeData(&$data)
	{
		$newData = array();
		foreach ($data AS $key => $value)
		{
			// check if the current value is an array itself
			// and in that case we extract it's key and value
			if (is_array($value))
			{
				list($key, $value) = array_values($value);
			}
			$newData[$key] = $value;
		}
		$data = $newData;
	}

	/**
	 * @see $_limit
	 * @return void
	 */
	public function setLimit($limit)
	{
		$this->_limit = (int) $limit;
	}

	/**
	 * @see $_offset
	 * @return void
	 */
	public function setOffset($offset)
	{
		$this->_offset = (int) $offset;
	}

	/**
	 * @see $_header
	 * @return String
	 */
	public function getHeader()
	{
		return $this->_header;
	}
	/**
	 * @see $_header
	 * @return void
	 */
	public function setHeader($str)
	{
		$this->_header = $str;
	}

	/**
	 * @see $_footer
	 * @return String
	 */
	public function getFooter()
	{
		return $this->_footer;
	}
	/**
	 * @see $_footer
	 * @return void
	 */
	public function setFooter($str)
	{
		$this->_footer = $str;
	}

	/**
	 * @see $_row
	 * @return String
	 */
	public function getRow()
	{
		return $this->_row;
	}
	/**
	 * @see $_row
	 * @return void
	 */
	public function setRow($str)
	{
		if (False === strpos($str, "%s"))
		{
			throw new Exception("There are no variables in the row of your table. Please use %s as variable");
		}
		$this->_keyValue = preg_match("/%s.*%s/s", $str);
		$this->_row = $str;
	}
}