<?php

  function getCurrentSong($player) {
    header("Content-Type: application/json");

    $current_song = "";
    if (count($player->tracks) > 0)
    {
      unset($player->tracks[0]);
      if (isset($player->tracks[1]))
        $player->tracks[1]['track'] = str_replace("Current Song", "", $player->tracks[1]['track']);
        $current_song = $player->tracks[1]['track'];
    }
    print json_encode(array("CurrentSong" => $current_song));
  }
?>