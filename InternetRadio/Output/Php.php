<?php
/**
 * Excudo InternetRadio
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://devshed.excudo.net/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to marty@excudo.net so we can send you a copy immediately.
 *
 * @category   Excudo
 * @package    InternetRadio
 * @copyright  Copyright (c) 2005-2010 Excudo. (http://www.excudo.net)
 * @license    http://devshed.excudo.net/license/new-bsd     New BSD License
 */
 
/**
 * @see InternetRadio_Output_Interface
 */
require_once dirname(__FILE__)."/Interface.php";

/**
 * This template class does nothing more than returning the passed data-array
 * It exists because the only way to force alternate output is to use a template
 * class of type InternetRadio_Output
 */
class InternetRadio_Output_Php implements InternetRadio_Output_Interface
{
	/**
	 * @param array $data	Array which we don't convert, but return immediately
	 *
	 * @return array
	 */
	public function render(array $data)
	{
		return $data;
	}
}
