<?php
/**
 * Excudo InternetRadio
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://devshed.excudo.net/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to marty@excudo.net so we can send you a copy immediately.
 *
 * @category   Excudo
 * @package    InternetRadio
 * @copyright  Copyright (c) 2005-2010 Excudo. (http://www.excudo.net)
 * @license    http://devshed.excudo.net/license/new-bsd     New BSD License
 */
 
/**
 * All output-templates must implement this interface to ensure that the render-method is available.
 * This way, any InternetRadio_Abstract class can set an object of this type as template and call
 * the render-method with the data as argument to generate the output
 */
interface InternetRadio_Output_Interface
{
	/**
	 * Implemenation of this method must parse the passed data and convert it to the desired output
	 * and then return that output
	 *
	 * @return String
	 */
	public function render(array $data);
}