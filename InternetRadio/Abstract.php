<?php
/**
 * Excudo InternetRadio
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://devshed.excudo.net/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to marty@excudo.net so we can send you a copy immediately.
 *
 * @category   Excudo
 * @package    InternetRadio
 * @copyright  Copyright (c) 2005-2010 Excudo. (http://www.excudo.net)
 * @license    http://devshed.excudo.net/license/new-bsd     New BSD License
 */

/**
 * @see InternetRadio_Exception
 */
require_once "Exception.php";

/**
 * Contains blueprint for some necessary functions as well as most of the functionality needed
 * to parse the online information of an internet radiostation
 */
abstract class InternetRadio_Abstract
{
	/**
	 * This value represents the case in which we want any possible exception to be thrown.
	 * @see $exceptionReporting
	 */
	const EXCEPTION_THROW	= 1;
	/**
	 * This value represents the case in which we want any possible exception to caught, but shown
	 * @see $exceptionReporting
	 */
	const EXCEPTION_SHOW	= 2;
	/**
	 * This value represents the case in which we want any possible exception to caught and hidden
	 * @see $exceptionReporting
	 */
	const EXCEPTION_HIDE	= 3;

	/**
	 * This setting determines what will be done when a exception occurs
	 * This only applies to exceptions that are thrown during the retrieval of information. Other
	 * exceptions, for example during the parsing of that information, will always be thrown.
	 *
	 * @see getExceptionReporting()
	 * @see setExceptionReporting()
	 *
	 * @var integer
	 */
	protected $exceptionReporting = self::EXCEPTION_HIDE;

	/**
	 * The domain where the radiostation can be found
	 *
	 * @var String
	 */
	public $domain;

	/**
	 * The port at which it can be found
	 *
	 * @var String
	 */
	public $port;

	/**
	 * The path where we can find the output.
	 * This is the path where the server/stream information can be found
	 *
	 * @var String
	 */
	public $path;

	/**
	 * This array will be filled with the applicable fields (which are different for every server-type)
	 * It can only contain key-value pairs of which the key also exist in the $defaultFields array
	 * of the implementing class
	 *
	 * @see setFields()
	 *
	 * @var array
	 */
	public $fields = array();

	/**
	 * Array which is going to store different kinds of content (depending on which type of content
	 * is loaded). In order to display information about the radio-station, different kinds of
	 * content may be loaded from different paths on the server. Whenever data has been loaded, it
	 * will be cached in this array.
	 *
	 * @see setContentArr()
	 *
	 * @var Array
	 */
	protected $contentArr;

	/**
	 * Different kinds of output-templates can be set in order to display the information with a
	 * certain markup. These templates can be passed as an argument whenever output is required,
	 * but you can set a default one as well, which is stored in this variable.
	 *
	 * @see setOutputTemplate()
	 *
	 * @var InternetRadio_Output_Interface
	 */
	protected $outputTemplate;

	/**
	 * Flag to toggle the creation of hyperlinks in the output of the server-information.
	 * By default this is not something we want
	 *
	 * @see setCreateHyperlinks()
	 *
	 * @var boolean
	 */
	protected $createHyperlinks = False;

	/**
	 * By setting this property you can indicate what the character encoding is, that
	 * the server uses in the public html pages with information.
	 * Note: an attempt will be made to auto-detect the character encoding, but this
	 * doesn't always work. This setting will always overrule that attempt though.
	 *
	 * @see setInputEncoding()
	 *
	 * @var String
	 */
	protected $inputEncoding;

	/**
	 * By setting this property you can decide how the output should be encoded. For
	 * example: if the stream-information is encoded 'iso-8859-1' and the website
	 * on which you want to display it is 'utf-8', then you should set this property
	 * to 'utf-8'
	 *
	 * @see setOutputEncoding()
	 *
	 * @var String
	 */
	protected $outputEncoding;

	/**
	 * An attempt will be made to auto-detect the encoding of the stream. (Currently, this
	 * only works for shoutcast server). When the attempt is succesfull, it will be
	 * saved in this property.
	 * Unless the $inputEncoding is set manually, this value will be treated as the
	 * inputEncoding
	 *
	 * @see getStreamEncoding()
	 *
	 * @var String
	 */
	protected $streamEncoding;


	/**
	 * Abstract functions
	 */

	/**
	 * Implementation of this function should parse the html with server/stream info
	 * and fill the internal $fields array with it.
	 *
	 * @return void
	 */
	abstract protected function parseFields();

	/**
	 * Implementation of this function should return what type of InternetRadiostation
	 * the class can handle
	 *
	 * @return String
	 */
	abstract public function getServerType();

	/**
	 * Implementation of this function should assign an array to internal $contentArr
	 * which contains the different kinds of content we can retrieve. These types
	 * should be the keys in the array and their values should be null.
	 * When that particular type of content has been loaded, null will be overwritten
	 * with the retrieved content.
	 *
	 * example: $this->contentArr = array("stream" => null, "history" => null);
	 *
	 * @return void
	 */
	abstract protected function setContentArr();

	/**
	 *
	 *
	 * @return void
	 */
	abstract protected function setPageMap();

	/**
	 * Every extended class should define this method in such a way that by default it set the
	 * $streamChunks array for the different kinds of information that can be retrieved.
	 * The array consists of the index under which the contents of a page are cached (and loaded)
	 * and the value represents how many chunks of data should be loaded by the fopen() command.
	 * This is because sometimes the data is embedded in a(n endless) stream in which case you
	 * want to limit it, to prevent it loads forever.
	 *
	 * @return void
	 */
	abstract protected function setStreamChunks();

	/**
	 * Public funtions
	 */

	/**
	 * Constructor.
	 *
	 * @return InternetRadio_Abstract
	 */
	public function __construct($url)
	{
		// parsing url and setting appro
		$parsed_url = parse_url($url);
		$this->domain	= isset($parsed_url['host']) ? $parsed_url['host'] : "";
		$this->port	= !isset($parsed_url['port']) || empty($parsed_url['port']) ? "80" : $parsed_url['port'];
		$this->path	= empty($parsed_url['path']) ? "/" : $parsed_url['path'];

		if (empty($this->domain))
		{
			$this->domain = $this->path;
			$this->path = "";
		}

		// setting default fields
		$this->setFields();
		// setting default page map
		$this->setPageMap();
		// setting default stream-chunks
		$this->setStreamChunks();

		// sets the indexes for all the different kinds of content
		$this->setContentArr();
		if (!is_array($this->contentArr))
			throw new InternetRadio_Exception("contentArr is not an array. Please implement setContentArr() properly.");
		elseif (count($this->contentArr) == 0)
			throw new InternetRadio_Exception("contentArr is empty. Please implement setContentArr() properly.");
		else
		{
			foreach ($this->contentArr AS $key => $value)
			{
				if (!is_null($value))
					throw new InternetRadio_Exception("Every value in the contentArr must have a default value of 'null'");
			}
		}

		// setting default output template
		require_once dirname(__FILE__)."/Output/Html/Table.php";
		$this->setOutputTemplate(new InternetRadio_Output_Html_Table());
	}

	/**
	 * Retrieves the information about the server/stream and returns it using
	 * the choosen (or default) template
	 *
	 * @param String $page						The page where the information about
	 * 								the stream can be found. If null, the
	 * 								default will be used. See
	 * @param InternetRadio_Output_Interface $template		The template that has to be used when
	 * 								upon returning the data
	 *
	 * @return mixed	The output as rendered by the template object. In most cases
	 *			this will be a string, but it could just as easily be another
	 *			type (for example, InternerRadio_Output_Php returns an array)
	 */
	public function getServerInfo($page = null, InternetRadio_Output_Interface $template = null)
	{
		try {
			$this->parseFields($page);
		} catch (InternetRadio_Exception $e) {
			$this->error = $e->getMessage();
			switch ($this->exceptionReporting)
			{
				case self::EXCEPTION_THROW :
					throw $e;
					break;
				case self::EXCEPTION_SHOW :
					$this->fields = array("error" => $e->getMessage());
					break;
				case self::EXCEPTION_HIDE :
				default :
					$this->fields = array("error"	=> "failed to load stream");
					break;
			}
		}
		return $this->getInfo($this->fields, $template);
	}

	/**
	 * Same as getServerInfo($page, InternetRadio_Output_Html_Table()), but without the
	 * need to pass the template object as an argument
	 *
	 * @param String $page
	 *
	 * @see getServerInfo()
	 *
	 * @return mixed
	 */
	public function getServerInfoAsHtml($page = null)
	{
		require_once dirname(__FILE__)."/Output/Html/Table.php";
		return $this->getServerInfo($page, new InternetRadio_Output_Html_Table());
	}
	/**
	 * Same as getServerInfo($page, InternetRadio_Output_Php()), but without the
	 * need to pass the template object as an argument
	 *
	 * @param String $page
	 *
	 * @see getServerInfo()
	 *
	 * @return mixed
	 */
	public function getServerInfoAsPhp($page = null)
	{
		require_once dirname(__FILE__)."/Output/Php.php";
		return $this->getServerInfo($page, new InternetRadio_Output_Php());
	}
	/**
	 * Same as getServerInfo($page, InternetRadio_Output_Xml()), but without the
	 * need to pass the template object as an argument
	 *
	 * @param String $page
	 *
	 * @see getServerInfo()
	 *
	 * @return mixed
	 */
	public function getServerInfoAsXml($page = null)
	{
		require_once dirname(__FILE__)."/Output/Xml.php";
		return $this->getServerInfo($page, new InternetRadio_Output_Xml());
	}

	/**
	 * By passing an array with field-names, this function can exercise control over which server-information
	 * will be displayed when getServerInfo() is called.
	 * When called without arguments (or boolean false as argument), all available fields will be shown
	 *
	 * @param array $array		Array of fields that we want to display when the server-info is requested
	 * 							These fields have to exist in the $defaultFields property
	 *
	 * @return void
	 */
	public function setFields($array=null)
	{
		if (is_null($array))
		{
			$this->fields = $this->defaultFields;
		}
		else
		{
			$tmpArr = array();
			// before setting the array, we check if it contains invalid fields
			foreach ($array AS $key => $value)
			{
				if (array_key_exists($key, $this->defaultFields))
				{
					$tmpArr[$key] = "n/a";
				}
				elseif (array_key_exists($value, $this->defaultFields))
				{
					$tmpArr[$value] = "n/a";
				}
				else
				{
					throw new InternetRadio_Exception($key."/".$value." is not a valid field (".implode(", ", array_keys($this->defaultFields)).")");
				}
			}
			$this->fields = $tmpArr;
		}
	}

	/**
	 * Influences how the urls in the server information are displayed.
	 *
	 * @param boolean $bool	If true, the returned server-information will show urls as hyperlinks
	 *
	 * @see parseFields()
	 * @see getServerInfo()
	 *
	 * @return void
	 */
	public function setCreateHyperlinks($bool)
	{
		$this->createHyperlinks = (bool) $bool;
	}

	/**
	 * Getter for $outputTemplate
	 *
	 * @return InternetRadio_Output_Interface
	 */
	public function getOutputTemplate()
	{
		return $this->outputTemplate;
	}
	/**
	 * Setter for $outputTemplate
	 *
	 * @param InternetRadio_Output_Interface $template
	 *
	 * @return void
	 */
	public function setOutputTemplate(InternetRadio_Output_Interface $template)
	{
		$this->outputTemplate = $template;
	}

	/**
	 * Getter for $exceptionReporting
	 *
	 * @see self::EXCEPTION_THROW, self::EXCEPTION_SHOW, self::EXCEPTION_HIDE
	 *
	 * @return integer
	 */
	public function getExceptionReporting()
	{
		return $this->exceptionReporting;
	}
	/**
	 * Setter for $exceptionReporting
	 *
	 * @see self::EXCEPTION_THROW, self::EXCEPTION_SHOW, self::EXCEPTION_HIDE
	 *
	 * @return void
	 */
	public function setExceptionReporting($int)
	{
		$this->exceptionReporting = (int) $int;
	}

	/**
	 * Setter for the outputEncoding
	 *
	 * @see $outputEncoding
	 *
	 * @return void
	 */
	public function setOutputEncoding($charset)
	{
		$this->outputEncoding = $charset;
	}
	/**
	 * Setter for the inputEncoding
	 *
	 * @see $inputEncoding
	 *
	 * @return void
	 */
	public function setInputEncoding($charset)
	{
		$this->inputEncoding = $charset;
	}
	/**
	 * Getter for the streamEncoding
	 *
	 * @see $streamEncoding
	 *
	 * @return void
	 */
	public function getStreamEncoding()
	{
		return $this->streamEncoding;
	}

	/**
	 * internal methods
	 */

	/**
	 * Setter for the streamEncoding
	 * This setter is not public because only the inputEncoding may be manipulated
	 * when working with an object of this class.
	 *
	 * @see $streamEncoding
	 * @see $inputEncoding
	 *
	 * @return void
	 */
	protected function setStreamEncoding($charset)
	{
		$this->streamEncoding = $charset;
	}
	
	/**
	 * Wrapper method for getContents, which fills the first argument correctly
	 *
	 * @param String $page		The location of the page with the content. This is the
	 * 							[page] part in http://[domain]:[port]/[page]
	 *
	 * @see getContents()
	 *
	 * @return String
	 */
	protected function getStreamContents($page = null)
	{
		return $this->getContents("stream", $page);
	}

	/**
	 * Loads the contents of the stream (/server) information of a radiostationg.
	 *
	 * @param String $page		The location of the page with the content. This is the
	 * 							[page] part in http://[domain]:[port]/[page]
	 *
	 * @throws InternetRadio_Exception
	 *
	 * @see loadContents()
	 *
	 * @return void
	 */
	protected function loadStreamContents($page = null)
	{
		$this->loadContents("stream", $page);
	}

	/**
	 * This function checks if loadContents() has been called before and either returns
	 * the cached information if it has, or otherwise calls loadContents and then
	 * returns the info
	 *
	 * @param String $index		This indicates what kind of content we're downloading.
	 * 							By naming it, we are able to cache it. (usually a server
	 * 							has more than 1 page with information)
	 * @param String $page		The location of the page with the content. This is the
	 * 							[page] part in http://[domain]:[port]/[page]
	 *
	 * @see loadContents()
	 *
	 * @return String
	 */
	protected function getContents($index, $page = null)
	{
		if (is_null($this->contentArr[$index]))
		{
			$this->loadContents($index, $page);
		}
		return $this->contentArr[$index];
	}

	/**
	 * This method contains all the logic to download the information from the public
	 * pages of the internet-radiostation, which contain all the data we're interested
	 * in showing.
	 *
	 * Two other settings are important for correctly loading the content of a page:
	 * 1. The $streamChunks array. Every extended class should define this array for the
	 * different kinds of information that can be retrieved. The default is 20 and this will be
	 * if nothing else has been defined. This is because the server information is usually
	 * embedded in the radio-stream who's content is unlimited. By setting the streamChunks
	 * to -1, all the content of a certain page will be loaded. So, if the index (see index parameter)
	 * of a page is 'history' then you should set $streamChunks['history'] = -1 in the class.
	 * 2. The $pageMap array. This array can form a mapping for the type of content to the default page.
	 * For example, the history of a shoutcast-server is usually found at /played.html. So in the Shoutcast
	 * implementation of this class $pageMap['history'] = '/played.html'. This takes away the necessity
	 * to always pass the $page variable as an argument, while keeping the flexibility for instances
	 * where
	 *
	 * @param String $index		This indicates what kind of content we're downloading.
	 * 							By naming it, we are able to cache it. (usually a server
	 * 							has more than 1 page with information)
	 * @param String $page		The location of the page with the content. This is the
	 * 							[page] part in http://[domain]:[port]/[page]
	 *
	 * @see setPageMap()
	 * @see setStreamChunks()
	 *
	 * @throws InternetRadio_Exception
	 *
	 * @return void
	 */
	protected function loadContents($index, $page = null)
	{
		if (!array_key_exists($index, $this->contentArr))
		{
			throw new InternetRadio_Exception($index." is not a valid content-index. should be: ".implode(", ", array_keys($this->contentArr)));
		}
		if (is_null($page))
		{
			if (!isset($this->pageMap[$index]))
				throw new InternetRadio_Exception("There is no default page defined for ".$index);
			else
				$page = $this->pageMap[$index];
		}
		$this->contentArr[$index] = "";
		$domain = (substr($this->domain, 0, 7) == "http://") ? substr($this->domain, 7) : $this->domain;

		if (@$fp = fsockopen($domain, $this->port, $this->errno, $this->errstr, 2))
		{
			fputs($fp, "GET ".$page." HTTP/1.1\r\n".
				"User-Agent: Mozilla/4.0 (compatible; MSIE 5.5; Windows 98)\r\n".
				"Accept: */*\r\n".
				"Host: ".$domain."\r\n\r\n");

			$c = 0;
			$cMax = isset($this->streamChunks[$index]) ? $this->streamChunks[$index] : 20;
			while (!feof($fp) && ($cMax == -1 || $c <= $cMax))
			{
				$this->contentArr[$index] .= fgets($fp, 4096);
				$c++;
			}
			fclose ($fp);

			// this doesn't work for all server-types
			preg_match("/(meta http-equiv=\"Content-Type\" content=\"(.*); charset=(.*)\")/iU", $this->contentArr[$index], $matches);
			if (isset($matches[3]))
			{
				$this->setStreamEncoding($matches[3]);
			}
			$this->encodeContent($this->contentArr[$index]);
		}
		else
		{
			$errArr = error_get_last();
			throw new InternetRadio_Exception("couldn't open stream @ ".$domain.":".$this->port."; ERROR: ".$errArr['message'].", type[".$errArr['type']."] in ".$errArr['file']." on line ".$errArr['line']);
		}
	}

	/**
	 * Returns any kind of data-array using a template's render function.
	 * When no template is passed, if will use the default template (@see constructor)
	 *
	 * @param array $data						An array containing data with information about the radio-station.
	 * @param InternetRadio_Output_Interface $template	The template that has to be used when upon returning the data
	 *
	 * @return mixed	The output as rendered by the template object. In most cases
	 * 					this will be a string, but it could just as easily be another
	 * 					type (for example, InternerRadio_Output_Php returns an array)
	 */
	protected function getInfo(array $data, InternetRadio_Output_Interface $template = null)
	{
		if (!is_null($template))
		{
			$oldTemplate = $this->getOutputTemplate();
			$this->setOutputTemplate($template);
		}
		else
		{
			$template = $this->getOutputTemplate();
		}
		$return = $template->render($data);
		if (isset($oldTemplate))
		{
			$this->setOutputTemplate($oldTemplate);
		}
		return $return;
	}

	/**
	 * This method encodes the passed string, based on the settings of $inputEncoding and $outputEncoding
	 *
	 * @param String $content	The string which we want to encode
	 *
	 * @return void			(The argument is be passed by reference)
	 */
	protected function encodeContent(&$content)
	{
		// we only continue if the outputEncoding has been set
		if (!is_null($this->outputEncoding))
		{
			if (is_null($this->inputEncoding))
			{
				$this->inputEncoding = $this->streamEncoding;
			}
			if (!function_exists("iconv"))
			{
				if (empty($this->inputEncoding))
					$content = mb_convert_encoding($content, $this->outputEncoding);
				else
					$content = mb_convert_encoding($content, $this->outputEncoding, $this->inputEncoding);
			}
			else
			{
				if (empty($this->inputEncoding))
					$content = iconv(null, $this->outputEncoding, $content);
				else
					$content = iconv($this->inputEncoding, $this->outputEncoding, $content);
			}
		}
	}
}
?>