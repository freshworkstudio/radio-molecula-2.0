<?php
/**
 * Excudo InternetRadio
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://devshed.excudo.net/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to marty@excudo.net so we can send you a copy immediately.
 *
 * @category   Excudo
 * @package    InternetRadio
 * @copyright  Copyright (c) 2005-2010 Excudo. (http://www.excudo.net)
 * @license    http://devshed.excudo.net/license/new-bsd     New BSD License
 */

/**
 * @see InternetRadio_Abstract
 */
require_once "Abstract.php";

/**
 * Class that can parse and display the information of Icecast Servers
 */
class InternetRadio_Icecast extends InternetRadio_Abstract
{
	const SERVER_TYPE	= "Icecast";

	/**
	 * All the possible fields with information about the stream/server that can be loaded
	 * By default, this array is assigned to the $fields (see parent class) array. But, the
	 * $fields array can also be set with setFields() in order to display a particular set
	 * of fields
	 *
	 * @var array
	 *
	 * @see $fields
	 * @see setFields()
	 */
	protected $defaultFields = array(
					"Server Type" => "n/a",
					"Stream Status" => "n/a",
					"Listener Peak" => "n/a",
					"Stream Title" => "n/a",
					"Content Type" => "n/a",
					"Mount Started" => "n/a",
					"Stream Genre" => "n/a",
					"Stream Description" => "n/a",
					"Stream URL" => "n/a",
					"Current Song" => "n/a",
					"Current Listeners" => "n/a",
					"Bitrate" => "n/a",
					"Audio Info" => "n/a",
					);

	/**
	 * The default streamChunks which are loaded when setStreamChunks() is called without
	 * argument (which is done in the constructor of the Abstract parent classs)
	 *
	 * @var array
	 */
	private $defaultStreamChunks = array(
		"stream"	=> 20,
		"status"	=> -1
		);
	/**
	 * By default it will be assigned $defaultStreamChunks as value.
	 *
	 * @see setStreamChunks()
	 *
	 * @var array
	 */
	protected $streamChunks;

	/**
	 * The default pageMaps which is loaded when setPageMap() is called without
	 * argument (which is done in the constructor of the Abstract parent classs)
	 *
	 * @var array
	 */
	private $defaultPageMap = array(
		"stream"	=> "/",
		"status"	=> "/status.xsl"
		);
	/**
	 * By default it will be assigned $defaultPageMap as value.
	 *
	 * @see setPageMap()
	 *
	 * @var array
	 */
	protected $pageMap;

	/**
	 * When true the 'Audio Info' data will be parsed as well and saved
	 * in seperate bits of information: bitrate, channels & samplerate.
	 * Note: bitrate is already available from another field
	 *
	 * @var boolean
	 */
	protected $parseAudioInfo = True;

	/**
	 * Implementation of abstract parent methods
	 */

	/**
	 * Returns the type of InternetRadiostation this class can handle
	 *
	 * @return String
	 */
	public function getServerType()
	{
		return self::SERVER_TYPE;
	}

	/**
	 * When called without argument it sets the default pageMap.
	 * The page map links
	 *
	 * @see parent::setPageMap()
	 *
	 * @param array $data	Array containing the mapping
	 *
	 * @return void
	 */
	protected function setPageMap(array $data = null)
	{
		if (is_null($data))
			$data = $this->defaultPageMap;
		$this->pageMap = $data;
	}

	/**
	 * When called without argument it sets the default streamChunks.
	 *
	 * @see parent::setStreamChunks()
	 *
	 * @param array $data	Array containing the mapping
	 *
	 * @return void
	 */
	protected function setStreamChunks($data = null)
	{
		if (is_null($data))
			$data = $this->defaultStreamChunks;
		$this->streamChunks = $data;
	}

	/**
	 * Retrieves the page with the information about the server/stream, parses it and puts the
	 * parsed data into the appropriate fields
	 *
	 * @see getStreamContents()
	 * @see loadStatus()
	 */
	protected function parseFields($page = null)
	{
		$contents = $this->getStreamContents($page);
		if (False !== $contents)
		{
			$dataStr = str_replace("\r", "\n", str_replace("\r\n", "\n", $contents));
			$lines = explode("\n", $dataStr);
			foreach ($lines AS $line)
			{
				if ($dp = strpos($line, ":"))
				{
					$key = substr($line, 0, $dp);
					$value = trim(substr($line, ($dp+1)));
					if (preg_match("/genre/i", $key) && isset($this->fields['Stream Genre']))
						$this->fields['Stream Genre'] = $value;
					if (preg_match("/^server$/i", $key) && isset($this->fields['Server Type']))
						$this->fields['Server Type'] = $value;
					elseif (preg_match("/name/i", $key) && isset($this->fields['Stream Title']))
						$this->fields['Stream Title'] = $value;
					elseif (preg_match("/server/i", $key) && isset($this->fields['Stream Type']))
						$this->fields['Stream Type'] = $value;
					elseif (preg_match("/description/i", $key) && isset($this->fields['Stream Description']))
						$this->fields['Stream Description'] = $value;
					elseif (preg_match("/content-type/i", $key) && isset($this->fields['Content Type']))
						$this->fields['Content Type'] = $value;
					elseif (preg_match("/icy-br/i", $key))
					{
						if (isset($this->fields['Stream Genre']))
							$this->fields['Stream Status'] = "Stream is up at ".$value."kbps";
						if (isset($this->fields['Bitrate']))
							$this->fields['Bitrate'] = $value;
					}
					elseif (preg_match("/url/i", $key) && isset($this->fields['Stream URL']))
					{
						if ($this->createHyperlinks)
						{
							$this->fields['Stream URL'] = "<a href=\"".$value."\">".$value."</a>";
						}
						else
						{
							$this->fields['Stream URL'] = $value;
						}
					}
					elseif (preg_match("/ice-audio-info/i", $key) && isset($this->fields['Audio Info']))
					{
						if ($this->getParseAudioInfo())
						{
							$dataString = trim(str_replace("ice-", " ", $value));
							$dataArr = explode("; ", $dataString);
							foreach ($dataArr AS $data)
							{
								list($k, $v) = explode("=", $data);
								if ($k == "samplerate")
									$this->fields['Samplerate'] = $v;
								elseif ($k == "channels")
									$this->fields['Channels'] = $v;
							}
							unset($this->fields['Audio Info']);
						}
						else
						{
							$this->fields['Audio Info'] = str_replace("ice-", " ", $value);
						}
					}
				}
			}
			if (!$this->loadStatus())
			{
				trigger_error("couldn't find the current stream on the icecast-status-page", E_USER_NOTICE);
			}

			return True;
		}
		else
		{
			return False;
		}
	}

	/**
	 * This function will be called from the parent's constructor
	 * By setting the contentArr, it serves two purposes:
	 * 1. The keys of the array can later be used in validations that check if the
	 *    index is an existing one (so, an existing key of this array)
	 * 2. It initializes the array with null values. This way the caching-functions
	 *    will be able to tell if content has not been loaded or if it has been loaded
	 *    with empty data
	 *
	 * @see parent::contentArr
	 * @see parent::__construct()
	 *
	 * @return void
	 */
	protected function setContentArr()
	{
		$this->contentArr = array(
			"stream"	=> null,
			"status"	=> null,
			"history"	=> null,
			);
	}

	/**
	 * Other methods which are specific for this internet-radiostation
	 */

	/**
	 * Getter for $parseAudioInfo
	 *
	 * @see $parseAudioInfo
	 *
	 * @return boolean
	 */
	public function getParseAudioInfo()
	{
		return $this->parseAudioInfo;
	}
	/**
	 * Setter for $parseAudioInfo
	 *
	 * @param boolean $bool
	 *
	 * @see $parseAudioInfo
	 *
	 * @return void
	 */
	public function setParseAudioInfo($bool)
	{
		$this->parseAudioInfo = (bool) $bool;
	}

	/**
	 * Loads and parses the contents of the status-page
	 *
	 * @param String $page			The location of the page with the content. This is the
	 * 								[page] part in http://[domain]:[port]/[page]
	 * @param String $streamTitle	The title of the stream we're interested in
	 * 								If not passed, we will retrieve it either from the cache
	 * 								or load it from the server-information page
	 *
	 * @return boolean				True if status could be loaded properly
	 */
	protected function loadStatus($page = null, $streamTitle = null)
	{
		// if the streamtitle is not given, we retrieve it from the server-information
		if (is_null($streamTitle))
		{
			// if the field is not yet or empty, we load the data first
			if (!isset($this->fields['Stream Title']) || empty($this->fields['Stream Title']))
			{
				$this->parseFields();
				if (!isset($this->fields['Stream Title']))
				{
					// without streamtitle, we cannot load the status
					throw new InternetRadio_Exception("Stream Title couldn't be found in contents");
				}
			}
			$streamTitle = $this->fields['Stream Title'];
		}

		// retrieving the contents of the status page
		$contents = $this->getStatusContents($page);

		if (False !== strpos($contents, "<table"))
		{
			// the status page contains lots of html-tables with information about the various
			// available streams on this server
			$tables = explode("<table", $contents);
			foreach ($tables AS $table)
			{
				// check if the current table is the stream we're interested in
				if (preg_match("/(<td(.*)>".$streamTitle."<\/td>)/", $table))
				{
					// this is the one, parsing it's data now
					$rows = explode("<tr>", $table);
					foreach ($rows AS $row)
					{
						if (preg_match_all("/<td.*>(.*)<\/td>/siU", $row, $matches))
						{
							$type = trim(str_replace(":", "", $matches[1][0]));
							$value = $matches[1][1];
							if ($type == "Current Song" && isset($this->fields['Current Song']))
								$this->fields['Current Song'] = $value;
							elseif ($type == "Current Listeners" && isset($this->fields['Current Listeners']))
								$this->fields['Current Listeners'] = $value;
							elseif ($type == "Peak Listeners" && isset($this->fields['Listener Peak']))
								$this->fields['Listener Peak'] = $value;
							elseif ($type == "Mount started" && isset($this->fields['Mount Started']))
								$this->fields['Mount Started'] = $value;
						}
					}
					return True;
				}
			}
		}
		else
		{
			throw new Exception($page." is not a valid status page or unreachable");
		}
		return False;
	}

	/**
	 * Wrapper for parent::getContents() with the first parameter filled correctly
	 *
	 * @param String $page		The location of the page with the content. This is the
	 * 							[page] part in http://[domain]:[port]/[page]
	 *
	 * @see getContents()
	 *
	 * @return String
	 */
	protected function getStatusContents($page = null)
	{
		return $this->getContents("status", $page);
	}
}