<?php
/**
 * Excudo InternetRadio
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://devshed.excudo.net/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to marty@excudo.net so we can send you a copy immediately.
 *
 * @category   Excudo
 * @package    InternetRadio
 * @copyright  Copyright (c) 2005-2010 Excudo. (http://www.excudo.net)
 * @license    http://devshed.excudo.net/license/new-bsd     New BSD License
 */

error_reporting(E_ALL);
ini_set("display_errors", 1);

// include the class
include "../../../InternetRadio/Shoutcast.php";
// include file that contains de $url variable, which contains the url of the radiostation we want to connect to
include "../url.php";

$url = "http://207.200.96.231:8002";

// instantiate radiostation object
$InternetRadioStation = new InternetRadio_Shoutcast($url);

$InternetRadioStation->setOutputEncoding("utf-8");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<?php
// retrieve track history
$output = $InternetRadioStation->getHistoryInfo();
echo "Stream encoding: ".$InternetRadioStation->getStreamEncoding()."<br>\n";
// display track history
echo $output;
?>
</body>
</html>
