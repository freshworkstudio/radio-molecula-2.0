<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

// include the class
include "../../../InternetRadio/Shoutcast.php";
// include file that contains de $url variable, which contains the url of the radiostation we want to connect to
include "../url.php";

// instantiate radiostation object
$InternetRadioStation = new InternetRadio_Shoutcast($url);

// include de DefinitionList class
require_once '../../../InternetRadio/Output/Html/UnorderedList.php';
// create template object
$template = new InternetRadio_Output_Html_UnorderedList();

$template->setOffset(0);
$template->setLimit(7);
// display track history
echo $InternetRadioStation->getHistoryInfo(null, $template);

?>
