This example folder contains 3 folders in which code-examples for those
radiostations can be found. Examples can be executed in your browser
if the example folder and InternetRadio folder are copied to the same
folder on your webserver.

Every folder contains a 'simple.php' file, which is the most basic
example. You will probably want to start with that if you're new
to this script

NOTE: The Icecast and Steamcast folder only contain examples of how to
instantiate the object. Once you know that, all the examples in the
shoutcast folder apply as well.